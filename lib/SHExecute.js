import { spawnSync, spawn, exec } from 'node:child_process';

/**
* Kills a process and all child processes of a given process ID in Linux/Posix.
* @param {number} processPid - The process ID.
* @param {string} signal - Signal to send.
* @retruns {Promise<number[]>} array with killed pid numbers
*/
const killProcesses = (processPid, signal) => {
	const killed = [];
	return new Promise((resolve, reject) => {
		// Command to get child PIDs of the given process
		const cmd = `pgrep -P ${processPid}`;
		exec(cmd, (error, stdout, stderr) => {
			if (error) {
				reject(error);
				return;
			}
			if (stderr) {
				reject(new Error(stderr));
				return;
			}
			const pids = stdout.split(/\r?\n/).filter(pid => pid) || [];
			// Kill each child process
			try {
				for (const pid of pids) {
					process.kill(parseInt(pid), signal);
					killed.push(parseInt(pid));
				}
			} catch (err) {
				reject(err);
				return;
			}
			// Kill the parent process after all child processes have been killed
			try {
				process.kill(processPid, signal);
				killed.push(processPid);
			} catch (err) {
				reject(err);
				return;
			}
			resolve(killed);
		});
	});
}

class SHExecute {
	/**
	* @type {import('child_process').ChildProcess}
	*/
	#forcedKill = false;
	#proc;
	#command = '';
	#options = {};
	#stdout = '';
	#stderr = '';
	/**
	* @param {string} command - linux command to be executed
	* @param {import('./SHDispatch').SHOptions} [options] - ChildProcess options
	*/
	constructor(command, options = {}) {
		this.#command = command;
		this.#options = options;
		this.#proc = null;
		this.#stdout = '';
		this.#stderr = '';
	}
	/**
	* @param {string} [payload] - data to write
	* @retuns {Promise<object>}
	*/
	runSync(payload) {
		if (payload && typeof payload !== 'string') {
			throw new Error('Argument is not a string');
		}
		let { cwd, shell, env, stdio, detached } = this.#options;
		// pipe need to be set on stdin when posting a payload
		if (payload) stdio[0] = 'pipe';
		const input = payload || undefined;
		return spawnSync(this.#options.prefix, [this.#command], {
			cwd,
			shell,
			stdio,
			detached,
			windowsHide: true,
			env,
			input
		});
	}
	/**
	* @param {string} [payload] - data to write
	* @retuns {Promise<string>}
	*/
	run(payload) {
		let to = 0;
		if (payload && typeof payload !== 'string') {
			throw new Error('Argument is not a string');
		}
		if (this.#options.timeout) {
			to = this.#options.timeout;
		}
		let { cwd, shell, env, stdio } = this.#options;
		// pipe need to be set on stdin when posting a payload
		if (payload) stdio[0] = 'pipe';
		this.#proc = spawn(this.#options.prefix, [this.#command], {
			cwd,
			shell,
			stdio,
			windowsHide: true,
			env,
		});
		this.#proc.stdout?.on('data', (data) => {
			this.#stdout += data;
		});

		this.#proc.stderr?.on('data', (data) => {
			this.#stderr += data;
		});
		if (payload) {
			this.#proc.stdin.end(payload);
		}
		return new Promise((resolve, reject) => {
			let timeout;
			if (to > 0) {
				timeout = setTimeout(async () => {
					this.#proc.kill();
					reject(new Error(`Process timed out: ${this.#command}`));
				}, to); // options.timeout
			}
			this.#proc.on('close', (code) => {
				if (timeout) clearTimeout(timeout);
				if (this.#forcedKill) {
					// Resolve without content
					resolve();
					return;
				}
				if (code === 0) {
					resolve(this.#stdout.trim());
				} else {
					reject(new Error(`${code}: ${this.#command} "${this.#stderr.trim()}"`));
				}
			});

			this.#proc.on('error', (err) => {
				reject(err);
			});
		});
	}
	/**
	* Kill this process and possible child processes
	* @param {string} signal - kill signal
	* @returns {Promise<number[]>}
	*/
	async kill(signal = 'SIGTERM') {
		if (!this.#proc) throw new Error('Trying to kill a process without creating one.');
		if (!this.#proc.pid) throw new Error('The process pid is undefined.');
		this.#forcedKill = true;
		let res = [];
		try {
			// Try to kill pid 'childs'
			res = await killProcesses(this.#proc.pid, signal);
		} catch (_e) { }
		if (!res.includes(this.#proc.pid)) {
			// Kill self if I am not allready killed
			res.push(this.#proc.pid);
			this.#proc.kill(signal);
		}
		this.#proc = undefined;
		return res;
	}
}

export default SHExecute;


import SHExec from './SHExecute.js';
/**
 * @typedef {Object} SpawnSyncResponse
 * @property {number} status - The exit code of the child process. A value of `0` indicates success.
 * @property {Buffer|null} signal - The signal used to terminate the process, if any.
 * @property {Array<string|null>} output - An array containing the standard output and standard error of the child process.
 * @property {number} pid - The process ID of the child process.
 * @property {Buffer|null} stdout - The standard output of the child process.
 * @property {Buffer|null} stderr - The standard error of the child process.
 */
/**
 * Default options for the execution environment.
 *
 * @typedef {Object} SHOptions
 * @property {string} [cwd] - The current working directory.
 * @property {NodeJS.ProcessEnv} [env] - The environment variables.
 * @property {string} [shell] - The shell to use for execution.
 * @property {string} [prefix] - The prefix commands to ensure a safe execution environment. e.g: prefix: 'set -euo pipefail;/usr/bin/env',
 * @property {StdioOptions|StdioOption} [stdio] - The stdio configuration.
 * @property {number} [timeout] - default 20000: a timeout error is triggerd when a process execution time is exceeded, 0 is no timeout
 * @property {boolean} [detached] - default false, when true it runnes as a background process  
 */
/**
 * @typedef {('pipe' | 'ignore' | 'inherit' | number)} StdioOption
 * @description Defines the stdio configuration for each of the standard streams.
 * 
 * - 'pipe' creates a pipe between the child process and the parent process. 
 *   The parent end of the pipe is exposed as a property on the `ChildProcess` object.
 * - 'ignore' indicates that the child process's corresponding stdio file descriptor will be ignored.
 * - 'inherit' passes the corresponding stdio stream to/from the child process.
 * - Stream object to be used for the stdio stream.
 * - Positive integer representing a file descriptor to be used for the stdio stream.
 */
/**
 * @typedef {Array<StdioOption>|StdioOption} StdioOptions
 * @description 
 * Configures the stdio streams for the child process. This can be an array or a single StdioOption.
 * 
 * Array Form: Specify the configuration for [stdin, stdout, stderr].
 *   - If array length is more than 3, additional positions correspond to extra streams.
 * Single Value: This value will be applied to stdin, stdout, and stderr.
 * 
 * Examples:
 * - ['pipe', 'pipe', 'ignore']: Pipe stdin and stdout, ignore stderr.
 * - 'inherit': Inherit all stdio streams from the parent.
 */
/**
 * 'Code Safe' has own prop
 *
 * @param {any} o - object to examine
 * @param {string} p - property to look for
 * @returns {boolean}
 */
const hasProp = (o, p) => {
	if (typeof o === 'undefined') {
		return false;
	}
	return Object.prototype.hasOwnProperty.call(o, p);
};

/**
* Merge property values while maintaining the fixed set of props in the original object
* @param {SHOptions} predefined - original object
* @param {SHOptions} options - object with new values
* @returns {SHOptions}
*/
const mergeOptions = (predefined, options) => {
	// Extract the keys from the predefined object
	const keys = Object.keys(predefined);

	// Use reduce to accumulate only the predefined properties from sourceObj
	const mergedObj = keys.reduce((acc, key) => {
		if (hasProp(options, key)) {
			acc[key] = options[key];
		} else {
			acc[key] = predefined[key];
		}
		return acc;
	}, {});

	return mergedObj;
}


/** @type {SHOptions} */
const defaultOptions = {
	cwd: process.cwd(),
	env: process.env,
	shell: 'bash',
	detached: false,
	prefix: '/usr/bin/env',
	stdio: ['inherit', 'pipe', 'pipe'],
	timeout: 10000 // when 0 there is no timeout
};



class SHDispatch {
	#cmd = '';
	#options = {};
	/** 
	* @type {SHExec} 
	*/
	#proc;
	/**
	* @param {string} cmd - cmd to execute
	*/
	constructor(cmd) {
		this.#cmd = cmd;
		this.#options = defaultOptions
	}
	/**
	* @param {SHOptions} options
	* @returns {SHDispatch}
	*/
	options(options) {
		if (options.stdio && typeof options.stdio === 'string') {
			// convert stdio to array
			// This sets the default io values
			// but can be overwritten when having a payload
			const io = options.stdio;
			options.stdio = Array(3).fill(io);
		}
		this.#options = mergeOptions(defaultOptions, options)
		return this;
	}
	/** 
	* @param {string} [payload]
	* @returns {Promise<string>}
	*/
	run(payload) {
		this.#proc = new SHExec(this.#cmd, this.#options);
		return this.#proc.run(payload);
	}

	/** 
	* Works for screen takeovers like editors
	* @param {string} [payload]
	* @returns {SpawnSyncResponse}
	*/
	runSync(payload) {
		// @ts-ignore
		return new SHExec(this.#cmd, this.#options).runSync(payload);
	}
	async kill(signal = 'SIGTERM') {
		let res = [];
		res = await this.#proc.kill(signal);
		this.#proc = undefined;
		return res;
	}
}

export default SHDispatch

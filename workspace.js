#!/usr/bin/env node
/*
# Workspace

This script sets up a tmux session with multiple windows.

```
   +---+---+
   |   0   |
   +---+---+
   | 1 | 2 |
   +---+---+
```
0. Main Editor Window
1. CLI Test/Commands Window
2. another window

This script resizes the tmux session to fit the available screen size and prepares applications/tools for use in this session.
*/
import { SH } from './lib/SH.js';

const SESSION = 'SH';
const heightp = 75;

const tmux = process.env.TMUX;
if (tmux) throw 'Do not run this in a tmux session';
try {
  const sessions = await SH`tmux ls`.run();
  if (sessions.includes(`${SESSION}:`)) {
    console.log(`Session ${SESSION} already created`);
    process.exit(1);
  }
} catch (_e) { }
const lines = parseInt(await SH`tput lines`.run());
const cols = parseInt(await SH`tput cols`.run());
// # Calculate height based on available line height
const HEIGHT = Math.ceil(lines * heightp / 100);
// Create tmux layout
const flags = [
  '-d',
  '-s',
  SESSION,
  '-x',
  `${cols}`,
  '-y',
  `${lines}`
];
await SH`tmux new-session ${flags}`.run();
// Set the environment BEFORE creating any panes
// await SH`tmux setenv -t ${SESSION} WORKSPACE_SOCKET ${socketFile}`.run();
// 
await SH`tmux rename-window 'edit'`.run();
// # create 2 rows
await SH`tmux split-window -v`.run();
// Set the percentage height calculated
await SH`tmux resize-pane -t ${SESSION}:0.0 -y ${HEIGHT}`.run();
await SH`tmux select-pane -t ${SESSION}:0.1`.run();
// # Focus on the second pane
// # Devide row 1 in 2 panes
await SH`tmux split-window -h`.run();
// await SH`tmux send-keys -t ${SESSION}:0.2 'joai ${EXPERT} --socket ${socketFile}' C-m`;
// set focus on main window
await SH`tmux select-window -t ${SESSION}:0`.run();
// Select main pane
await SH`tmux select-pane -t ${SESSION}:0.0`.run();
// attach to session, and keep running
await SH`tmux attach -t ${SESSION}`.options({timeout: 0}).run();

#! /usr/bin/env node
import assert from 'node:assert/strict';
import { SH, within, sleep } from '../lib/SH.js';

/**
 * Splits a command string into an array of arguments, handling quoted strings.
 * This fixes a problem when a command is like this: SH`${command}` 
 * 
 * @param {string} command - The command string to split.
 * @returns {string[]} - The array of command arguments.
 */
const args = (command) => {
	const args = [];
	let currentArg = '';
	let insideQuotes = false;
	let quoteChar = null;

	for (let i = 0; i < command.length; i++) {
		const char = command[i];

		if (char === '"' || char === "'") {
			if (insideQuotes && quoteChar === char && command[i - 1] !== '\\') {
				insideQuotes = false;
				quoteChar = null;
			} else if (!insideQuotes) {
				insideQuotes = true;
				quoteChar = char;
			} else {
				currentArg += char;
			}
		} else if (char === ' ' && !insideQuotes) {
			if (currentArg.length > 0) {
				args.push(currentArg);
				currentArg = '';
			}
		} else {
			currentArg += char;
		}
	}

	if (currentArg.length > 0) {
		args.push(currentArg);
	}

	return args.map(arg => arg.replace(/\\(['"])/g, '$1'));
}

/**
* escape paramater commands
* @param {string} arg
* @returns {string}
*/
const quote = (arg) => {
	if (/^[a-z0-9/_.\-@:=]+$/i.test(arg) || arg === '') {
		return arg;
	}
	return (`'` +
		arg
			.replace(/\\/g, '\\\\')
			.replace(/'/g, "\\'")
			.replace(/\f/g, '\\f')
			.replace(/\n/g, '\\n')
			.replace(/\r/g, '\\r')
			.replace(/\t/g, '\\t')
			.replace(/\v/g, '\\v')
			.replace(/\0/g, '\\0') +
		`'`);
}

/**
* escape paramater commands
* @param {string} arg
* @returns {string}
*/
const escape = (arg) => {
	if (/^[a-z0-9/_.\-@:=]+$/i.test(arg) || arg === '') {
		return arg;
	}
	return (
		arg
			.replace(/\\/g, '\\\\')
			// .replace(/'/g, "\\'")
			.replace(/\f/g, '\\f')
			.replace(/\n/g, '\\n')
			.replace(/\r/g, '\\r')
			.replace(/\t/g, '\\t')
			.replace(/\v/g, '\\v')
			.replace(/\0/g, '\\0')
	);
}

const escapeNew = (arg) => {
	if (/^[a-z0-9/_.\-@:=]+$/i.test(arg) || arg === '') {
		return arg;
	}
	return (
		arg
			.replace(/\\/g, '\\\\')
			.replace(/"/g, '\\"')
			.replace(/'/g, "\\'")
			.replace(/`/g, '\\`')
			.replace(/\f/g, '\\f')
			.replace(/\n/g, '\\n')
			.replace(/\r/g, '\\r')
			.replace(/\t/g, '\\t')
			.replace(/\v/g, '\\v')
			.replace(/\0/g, '\\0')
	);
}
const long =
	`
\`\`\`javascript
/**
* This function reads the standard input (stdin) from the current process.
* @example 
* const content = await stdin();
*/
const readIn = async () => {
	let buf = '';
	process.stdin.setEncoding('utf8');
	for await (const chunk of process.stdin) {
		buf += chunk;
	}
	return buf;
}
\`\`\`

This is an example
`
const cmd = `echo "${long}" > test.txt`

// const largecmd = "echo '# Interacting with ONNX Models Using Zig\n\nAs of now, there is no native Zig library specifically designed for interacting with ONNX models. However, you can still use Zig to interact with ONNX models by leveraging existing ONNX runtimes and libraries written in other languages, such as C or C++, and interfacing with them from Zig.\n\n## Possible Approaches\n\n1. **Using ONNX Runtime C API**:\n   - ONNX Runtime provides a C API that you can call from Zig. You would need to write Zig bindings for the ONNX Runtime C API.\n   - This involves defining the necessary C function signatures in Zig and using Zig's `@cImport` and `@cInclude` to call these functions.\n\n2. **FFI (Foreign Function Interface)**:\n   - Use Zig's FFI capabilities to call functions from a compiled ONNX Runtime shared library (e.g., `onnxruntime.dll`, `libonnxruntime.so`).\n   - This approach requires understanding the ONNX Runtime API and correctly mapping the data structures and function calls in Zig.\n\n## Example Outline for Using ONNX Runtime C API in Zig\n\n1. **Install ONNX Runtime**:\n   - Download and install the ONNX Runtime library for your platform.\n\n2. **Create Zig Bindings**:\n   - Define the necessary C function signatures and data structures in Zig.\n\n3. **Call ONNX Runtime Functions**:\n   - Use the defined bindings to load the ONNX model, prepare input data, run inference, and process the output.\n\n## Sample Zig Code Snippet\n\n```zig\nconst std = @import(\"std\");\n\nconst onnxruntime = @cImport({\n    @cInclude(\"onnxruntime_c_api.h\");\n});\n\npub fn main() void {\n    const allocator: *onnxruntime.OrtAllocator = null;\n    const env: *onnxruntime.OrtEnv = null;\n    const session: *onnxruntime.OrtSession = null;\n    const session_options: *onnxruntime.OrtSessionOptions = null;\n\n    // Initialize ONNX Runtime\n    const api = onnxruntime.OrtGetApiBase().GetApi(onnxruntime.ORT_API_VERSION);\n    api.CreateEnv(onnxruntime.ORT_LOGGING_LEVEL_WARNING, \"test\", &env);\n    api.CreateSessionOptions(&session_options);\n\n    // Load the ONNX model\n    const model_path = \"path/to/your/model.onnx\";\n    api.CreateSession(env, model_path, session_options, &session);\n\n    // Prepare input data, run inference, and process output\n    // (Implementation details depend on your specific model and data)\n\n    // Clean up\n    api.ReleaseSession(session);\n    api.ReleaseSessionOptions(session_options);\n    api.ReleaseEnv(env);\n}\n```\n\n## Steps to Implement\n\n1. **Set Up Zig Project**:\n   - Create a new Zig project and configure the build.zig file to include the ONNX Runtime headers and link against the ONNX Runtime library.\n\n2. **Define Bindings**:\n   - Define the necessary C function signatures and data structures in a Zig file using `@cImport`.\n\n3. **Implement Inference Logic**:\n   - Write the logic to load the model, prepare input data, run inference, and process the output.\n\n## Resources\n\n- [ONNX Runtime C API Documentation](https://onnxruntime.ai/docs/api/c/)\n- [Zig Documentation on C Interop](https://ziglang.org/documentation/master/#C-Interop)\n' > test2.txt";
const echo = "echo 'atures in Zig and using Zig's `@cImport` and `@cInclude` to call these functions.\n\n2. **FFI (Foreign Function Interface)**:\n   - Use Zig's FFI capabilities to call functions from a compiled ONNX Runtime shared library (e.g., `onnxruntime.dll`, `libonnxruntime.so`).\n   - This approach requires understanding the ONNX Runtime API and correctly mapping the data structures and function calls in Zig.\n\n## Example Outline for Using ONNX Runtime C API in Zig\n\n1. **Install ONNX Runtime**:\n   - Download and install the ONNX Runtime library for your platform.\n\n2. **Create Zig Bindings**:\n   - Define the necessary C function signatures and data structures in Zig.\n\n3. **Call ONNX Runtime Functions**:\n   - Use the defined bindings to load the ONNX model, prepare input data, run inference, and process the output.\n\n## Sample Zig Code Snippet\n\n```zig\nconst std = @import(\"std\");\n\nconst onnxruntime = @cImport({\n    @cInclude(\"onnxruntime_c_api.h\");\n});\n\npub fn main() void {\n    const allocator: *onnxruntime.OrtAllocator = null;\n    const env: *onnxruntime.OrtEnv = null;\n    const session: *onnxruntime.OrtSession = null;\n    const session_options: *onnxruntime.OrtSessionOptions = null;\n\n    // Initialize ONNX Runtime\n    const api = onnxruntime.OrtGetApiBase().GetApi(onnxruntime.ORT_API_VERSION);\n    api.CreateEnv(onnxruntime.ORT_LOGGING_LEVEL_WARNING, \"test\", &env);\n    api.CreateSessionOptions(&session_options);\n\n    // Load the ONNX model\n    const model_path = \"path/to/your/model.onnx\";\n    api.CreateSession(env, model_path, session_options, &session);\n\n    // Prepare input data, run inference, and process output\n    // (Implementation details depend on your specific model and data)\n\n    // Clean up\n    api.ReleaseSession(session);\n    api.ReleaseSessionOptions(session_options);\n    api.ReleaseEnv(env);\n}\n```\n\n## Steps to Implement\n\n1. **Set Up Zig Project**:\n   - Create a new Zig project and configure the build.zig file to include the ONNX Runtime headers and link against the ONNX Runtime library.\n\n2. **Define Bindings**:\n   - Define the necessary C function signatures and data structures in a Zig file using `@cImport`.\n\n3. **Implement Inference Logic**:\n   - Write the logic to load the model, prepare input data, run inference, and process the output.\n\n## Resources\n\n- [ONNX Runtime C API Documentation](https://onnxruntime.ai/docs/api/c/)\n- [Zig Documentation on C Interop](https://ziglang.org/documentation/master/#C-Interop)\n'";

const lines = echo.split('\n');
console.log(lines);
const content = await SH`${lines}`;
// const content = await SH`${lines}`.run();
// console.log(content);
// const echo = "echo '## Resources\n\n- [ONNX Runtime C API Documentation](https://onnxruntime.ai/docs/api/c/)\n- [Zig Documentation on C Interop](https://ziglang.org/documentation/master/#C-Interop)\n (e.g., `onnxruntime.dll`, `libonnxruntime.so`).\n '";

// within(async () => {
// 	// This works
// 	// const command = args(cmd);
// 	// console.log(command);
//   // const test = ['echo' ,'"hello world"' , '>', 'test.txt'];
// 	// const content = await SH`${test}`.run();
// 	console.log('OKAY');
// 	const content = await SH`echo "hello world direct" > test.txt`.run();
// 	console.log(content);
// });
// within(async () => {
// 	// This works
// 	// const command = args(cmd);
//   // const test = ['echo' ,'"hello world"' , '>', 'test.txt'];
// 	// const content = await SH`${test}`.run();
// 	console.log('NOT OKAY');
// 	const content = await SH`${cmd}`.run();
// 	console.log(content);
// });
within(async () => {
	// This works
	// const command = args(cmd);
	// const command = cmd.split(' ');
	// console.log({command});
	// console.log(command);
	// const test = ['echo' ,'"hello world"' , '>', 'test.txt'];
	// const content = await SH`${escape(largecmd)}`.run();
	// console.log(escapeNew(echo));
});

// // Test cases
// const testCases = [
//         `"`,
//         "`",
//         `@cImport({ @cInclude("onnxruntime_c_api.h"); });`,
//         `const model_path = "path/to/your/model.onnx";`
// ];
// 
// testCases.forEach((testCase, index) => {
//         const escaped = escapeNew(testCase);
//         console.log(`Test Case ${index + 1}:`);
//         console.log(`Original: ${testCase}`);
//         console.log(`Escaped: ${escaped}`);
//         console.log('---');
// });
// 
// const esc1 = (arg) => {
//         if (/^[a-z0-9/_.\-@:=]+$/i.test(arg) || arg === '') {
//                 return arg;
//         }
//         return (
//                 arg
//                         .replace(/\\/g, '\\\\')
//                         .replace(/'/g, "'\\''")
//                         .replace(/"/g, '\\"')
//                         .replace(/`/g, '\\`')
//                         .replace(/\f/g, '\\f')
//                         .replace(/\n/g, '\\n')
//                         .replace(/\r/g, '\\r')
//                         .replace(/\t/g, '\\t')
//                         .replace(/\v/g, '\\v')
//                         .replace(/\0/g, '\\0')
//                 );
// }
// 
// // const problem = `echo 'const model_path = "path/to/your/model.onnx";'`;
// // console.log(problem);
// // const problematicString = esc1(`echo 'Use Zig's \`@cImport\` and \`@cInclude\` to call these functions.'`);
// // console.log(problematicString);
// 
// const problematicString = `echo 'Use Zig's \`@cImport\` and \`@cInclude\` to call these functions.'`;
// const escapedString = escapeNew(problematicString);
// 
// console.log(`Original: ${problematicString}`);
// console.log(`Escaped: ${escapedString}`);
// 

#! /usr/bin/env node
import { SH } from '../lib/SH.js';

import assert from 'node:assert';
(async () => {
	console.log('THIS SHOULD CAUSE A TIMEOUT ERROR AFTER 1000ms');
  const command = SH`sleep 10; echo 10`.options({ stdio: 'inherit', timeout: 1000 });
  try {
    await command.run()
  } catch(e) {
  	assert.strictEqual(e.toString(), 'Error: Process timed out: sleep 10; echo 10');
  }
})();
(async () => {
	console.log('KILL SWITCH TEST');
  const command = SH`sleep 10; echo 10`.options({ stdio: 'inherit'});
  command.run()
	const pid = await command.kill();
  assert.strictEqual(pid.length, 2)
})();

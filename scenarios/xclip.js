#! /usr/bin/env node
import assert from 'node:assert/strict';
import { SH } from '../lib/SH.js';

/**
* Copy text to the clipboard
* @param {string} text
* @retruns {Promise<string>}
*/
const copyToClipboard = async (text) => {
	const prams = [
		'-selection',
		'clipboard'
	]
	return SH`xclip ${prams}`.options({ stdio: 'inherit' }).run(text);
}
(async () => {
	await copyToClipboard('This text');
	const params = ['-selection', 'clipboard', '-o',];
	// paste from clipboard
	const content = await SH`xclip ${params}`.run();
	assert.equal(content, 'This text');
})();

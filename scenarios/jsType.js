#! /usr/bin/env node
import assert from 'node:assert/strict';
import { jsType, Test, SH } from '../lib/SH.js';

assert.strictEqual(jsType(new Promise((_resolve, _reject) => {})), 'Promise');
assert.strictEqual(jsType(''), 'String');
assert.strictEqual(jsType(1), 'Number');
assert.strictEqual(jsType(1.001), 'Number');
assert.strictEqual(jsType(new Date()), 'Date');
assert.strictEqual(jsType(() => {console.log('DO NOT RUN')}), 'Function');
assert.strictEqual(jsType(async () => {}), 'AsyncFunction');
assert.strictEqual(jsType(new Test()), 'Test');
assert.strictEqual(jsType({}), 'Object');
assert.strictEqual(jsType(SH``), 'SHDispatch');
assert.strictEqual(jsType(SH``.run()), 'Promise');


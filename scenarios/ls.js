#! /usr/bin/env node
import { SH } from '../lib/SH.js';
// For a screen take over: stdin, stdout and stderr needs to be on 'inherit'
const res = await SH`ls -Fla`.run();
console.log(res);

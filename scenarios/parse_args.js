#! /usr/bin/env node
import assert from 'node:assert/strict';
import { SH, within, parseArgs } from '../lib/SH.js';

within(async () => {
	const test = ['--help','--item','manual','multiple', 'words', '--sentence', 'Hello sunshine'];
	const testArgs = parseArgs(test);
	assert.equal(testArgs.help, true);
	assert.equal(testArgs.item, 'manual');
	assert.equal(testArgs.sentence, 'Hello sunshine');
	assert.equal(testArgs._[0], 'multiple');
	assert.equal(testArgs._[1], 'words');
});


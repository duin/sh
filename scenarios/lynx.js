#! /usr/bin/env node
import { SH } from '../lib/SH.js';

(async () => {
  const prms = [
    '-dump',
    '-accept_all_cookies',
    '-hiddenlinks=ignore',
    '-unique_urls',
    '-trim_blank_lines',
    // 'https://search.brave.com/search?q=The+most+popular+synthesizers+from+Behringer'
    'https://duckduckgo.com/?q=The+most+popular+synthesizers+from+Behringer'
  ];
  const res = await SH`lynx ${prms}`.run();
  console.log(res);
})();

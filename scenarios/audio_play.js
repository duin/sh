#! /usr/bin/env node
import path from 'node:path';
import assert from 'node:assert/strict';
import { SH } from '../lib/SH.js';

// play audio, and stop playing
let command

(async () => {
	const file1 = path.resolve('scenarios', '1733488195475.mp3');
	command = SH`sox ${file1} -d tempo 1.4`.options({timeout: 0});
	command.run().then((res) => {
	});
	setTimeout(async () => {
	    let res = await command.kill('SIGKILL');
	    assert.strictEqual(res.length, 1);
	}, 800);
})();

#! /usr/bin/env node
/*
* Test the test
*/
import { assert, jsType, Test, within} from '../lib/SH.js';
const test = new Test();
test.syncTimeout(600);
test.add('Test is test in sync', () => {
	assert.strictEqual(jsType(test), 'Test');
});
test.add('Test an Array in sync', () => {
	assert.strictEqual(jsType([]), 'Array');
}).add('Test an Array in sync, with an external call stack', () => {
	setTimeout(() => {
		assert.strictEqual('A', 'A');
	}, 500);
});
test.add('Test an Array in sync, with an error in external call stack', () => {
	setTimeout(() => {
		console.log('-- THIS ERROR IS ON PURPOSE        --');
		console.log('-- It should be thown, but not within this test --');
		// The time-out does throw later when the test is allready finished
		assert.strictEqual('A', 'B');
	}, 500);
});
test.add('Test is test in async', async () => {
	assert.strictEqual(jsType(test), 'Test');
});
test.add('Test is test in async, returning a promise', () => {
	return new Promise((resolve, reject) => {
		assert.strictEqual(jsType(test), 'Test');
		setTimeout(() => {
			within(() => {
				console.log('-- THIS ERROR IS ON PURPOSE        --');
				console.log('-- It should be thown in THIS test --');
				console.log('-- It DOES NOT RESOLVE --');
				assert.strictEqual('C', 'D');
				// This will be present as argument in the resolve callback
			}, resolve, reject);
		}, 500)
	});
});
test.add('Silly async returning a Pomise', async () => {
	return new Promise((resolve, _reject) => {
		assert.strictEqual(jsType(test), 'Test');
		resolve();
	});
});
const report = await test.run();
console.log(report);


#! /usr/bin/env node
import assert from 'node:assert/strict';
import { SH, within, sleep } from '../lib/SH.js';

within(async () => {
	// This works
	const content = await SH`uname -r`.run();
	console.log(content);
});

within(async () => {
	// This works
	const command = "uname -r";
	const content = await SH`${command}`.run();
	console.log(content);
});
await sleep(1);

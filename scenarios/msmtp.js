#! /usr/bin/env node
import assert from 'node:assert/strict';
import { SH, within, sleep } from '../lib/SH.js';

const subject = 'Test email module MSMTP'
const text = `
I am multi line text
Do you copy?
`.trim()
const to = 'jd@dgebv.nl';
within(async () => {
	// This works
  const res = await SH`msmtp ${to}`.run(`Subject: ${subject}\n\n${text}`);
	console.log(res);
});

await sleep(1);

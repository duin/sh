#! /usr/bin/env node
import assert from 'node:assert/strict';
import { SH, within, sleep, retry, expBackoff, Test } from '../lib/SH.js';

const test = new Test();
test.add('export VAR', async () => {
	assert.rejects(async () => {
		await SH`export TEST_DEV=1`.run();
	})
});
test.add('Bash piping example', async () => {
	const res = await SH`ls -FLa | grep package.json | wc -l`.run();
	assert.equal(res, '1');
});
test.add('Pipe text to command', async () => {
	const res = await SH`wc -l`.run(`one\ntwo\n`);
	assert.equal(res, '2');
})
test.add('cat package.json', async () => {
	const res = await SH`ls -FLa | grep package.json`.run();
	assert.equal(res.trim(), 'package.json');
})
test.add('Create an async context, pass in a promise to simulate a sync callstack',
	async () => {
		const res = await Promise.all([
			SH`sleep 1; echo 10`.run(),
			SH`sleep 2; echo 99`.run(),
			sleep(1)
		]);
		assert.equal(res.length, 3);
		// Obtain the value from "echo"
		assert.equal(res[0].toString().trim(), '10');
		assert.equal(res[1].toString().trim(), '99');
	}
);
/*
* This test has a sync function
* However, it is creating an async context 'within'
* 
*/
test.add('Test async within', () => {
	// Get a result from an async block
	within(async () => {
		assert.equal(1, 1);
		return 'ok';
	}, (res) => {
		assert.equal(res, 'ok');
	}, () => {
		// This should not be called (reject handler)
		assert.equal(1, 2);
	})
});


test.add('Test sync within', () => {
	// Get a result from an async block
	within(() => {
		assert.equal(1, 1);
		return 'ok';
	}, (res) => {
		assert.equal(res, 'ok');
	}, function reject(_err) {
		throw new Error('I should not be called');
	})
}),

 test.add('Test within, catching error', () => {
 	// Get a result from an async block
 	within(async () => {
 		// This causes an error
 		assert.equal(1, 2);
 		return 'ok';
 	}, function resolve(_res) {
 		 // This slould not be called
		throw new Error('I should not be called');
 	}, function reject(err) {
 		// The error is checked here
 		assert.ok(err.toString().trim().includes('1 !== 2'));
 	})
 });
test.add('Handle errors', async () => {
	let error = false;
	try {
		await retry(3, expBackoff(), () => SH`curl -s https://flipwrsi`.run());
	} catch (e) {
		error = true;
		assert.equal(e instanceof Error, true);
	}
	assert.ok(error);
});

test.add('test retry', async () => {
	let counter = 0;
	try {
		const p = await retry(4, '1s', () => {
			// console.log({ counter });
			counter = counter + 1;;
			if (counter < 3) {
				throw new Error('Not yet');
			}
			return 'a';
		});
		assert.equal(p, 'a');
		assert.equal(true, false, 'should have thrown');
	} catch (e) {
		assert.equal(e instanceof Error, true);
	}
});
test.add('test flags as array', async () => {
	const flags = ['-F', '-l', '-a']
	const res = await SH`ls ${flags} | grep README`.run();
	assert.deepEqual(res.includes('README.md'), true);
});

test.add('Create and handle an error', async () => {
	try {
		await SH`$$BREAK IT`.run();
		assert.equal(true, false, 'should have thrown');
	} catch (e) {
		assert.equal(e instanceof Error, true);
	}
})
test.add('Test kill on bash', async () => {
	return new Promise((resolve, reject) => {
		const p = SH`sleep 5; echo 1`;
		const res = p.run();
		setTimeout(async () => {
			// throws
			const res = await p.kill();
			assert.equal(res.length, 2);
			resolve();
		}, 50);
		res.catch((e) => {
			assert.equal(e instanceof Error, true);
			reject(e);
		});
	});
}
);

const report = await test.run();
if (report.errors > 0) {
	process.exit(1);
}

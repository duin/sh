export default Test;
export type AsyncFunction = (() => Promise<any>);
export type testDefinition = {
    description: string;
    /**
     * - syc/ async function
     */
    callback: Function | AsyncFunction;
};
export type testReport = {
    description: string;
    /**
     * - start time in MS
     */
    duration: number;
    /**
     * - Has it been called?
     */
    executed: boolean;
};
export type Report = {
    tests: number;
    /**
     * - start time in MS
     */
    duration: number;
    /**
     * - number of errors
     */
    errors: number;
    /**
     * - number of tests executed
     */
    executed: number;
};
declare class Test {
    /**
    * @param {boolean} [quiet] - does not output a report when true, default `false`
    */
    constructor(quiet?: boolean | undefined);
    /**
    * Set the timeout when a synced function is called.
    * This settles async code used in a sync function
    * and give some time to catch errors (#HACK)
    * @param {number} timeout - in MS, default 50
    */
    syncTimeout(timeout: number): void;
    /**
    *
    * @param {string} description
    * @param {Function|AsyncFunction} callback - sync / async function
    * @throws Error when conditions are not met
    * @returns {Test}
    */
    add(description: string, callback: Function | AsyncFunction): Test;
    /**
    * Execute tests
    * @param {number[]} [execute] - limit the execution tests
    * @returns {Promise<Report>}
    */
    run(execute?: number[] | undefined): Promise<Report>;
    /**
    * Empty tests
    */
    reset(): void;
    #private;
}

export default SHDispatch;
export type SpawnSyncResponse = {
    /**
     * - The exit code of the child process. A value of `0` indicates success.
     */
    status: number;
    /**
     * - The signal used to terminate the process, if any.
     */
    signal: Buffer | null;
    /**
     * - An array containing the standard output and standard error of the child process.
     */
    output: Array<string | null>;
    /**
     * - The process ID of the child process.
     */
    pid: number;
    /**
     * - The standard output of the child process.
     */
    stdout: Buffer | null;
    /**
     * - The standard error of the child process.
     */
    stderr: Buffer | null;
};
/**
 * Default options for the execution environment.
 */
export type SHOptions = {
    /**
     * - The current working directory.
     */
    cwd?: string | undefined;
    /**
     * - The environment variables.
     */
    env?: NodeJS.ProcessEnv | undefined;
    /**
     * - The shell to use for execution.
     */
    shell?: string | undefined;
    /**
     * - The prefix commands to ensure a safe execution environment. e.g: prefix: 'set -euo pipefail;/usr/bin/env',
     */
    prefix?: string | undefined;
    /**
     * - The stdio configuration.
     */
    stdio?: number | "pipe" | "ignore" | "inherit" | StdioOption[] | undefined;
    /**
     * - default 20000: a timeout error is triggerd when a process execution time is exceeded, 0 is no timeout
     */
    timeout?: number | undefined;
    /**
     * - default false, when true it runnes as a background process
     */
    detached?: boolean | undefined;
};
export type StdioOption = ("pipe" | "ignore" | "inherit" | number);
export type StdioOptions = Array<StdioOption> | StdioOption;
declare class SHDispatch {
    /**
    * @param {string} cmd - cmd to execute
    */
    constructor(cmd: string);
    /**
    * @param {SHOptions} options
    * @returns {SHDispatch}
    */
    options(options: SHOptions): SHDispatch;
    /**
    * @param {string} [payload]
    * @returns {Promise<string>}
    */
    run(payload?: string | undefined): Promise<string>;
    /**
    * Works for screen takeovers like editors
    * @param {string} [payload]
    * @returns {SpawnSyncResponse}
    */
    runSync(payload?: string | undefined): SpawnSyncResponse;
    kill(signal?: string): Promise<number[]>;
    #private;
}

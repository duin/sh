export default SHExecute;
declare class SHExecute {
    /**
    * @param {string} command - linux command to be executed
    * @param {import('./SHDispatch').SHOptions} [options] - ChildProcess options
    */
    constructor(command: string, options?: any);
    /**
    * @param {string} [payload] - data to write
    * @retuns {Promise<object>}
    */
    runSync(payload?: string | undefined): import("child_process").SpawnSyncReturns<Buffer> & import("child_process").SpawnSyncReturns<string> & import("child_process").SpawnSyncReturns<string | Buffer>;
    /**
    * @param {string} [payload] - data to write
    * @retuns {Promise<string>}
    */
    run(payload?: string | undefined): Promise<any>;
    /**
    * Kill this process and possible child processes
    * @param {string} signal - kill signal
    * @returns {Promise<number[]>}
    */
    kill(signal?: string): Promise<number[]>;
    #private;
}

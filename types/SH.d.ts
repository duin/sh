export type ArgsObject = {
    [x: string]: ApiMethod;
};
export type RejectCallback = Function;
export type ResolveCallback = Function;
/**
 * Creates a new SHDispatch object that represents a command to be executed.
 */
export type Shell = Function;
/** @type {Shell & { (pieces: TemplateStringsArray, ...args: *): SHDispatch }} */
export const SH: Shell & {
    (pieces: TemplateStringsArray, ...args: any): SHDispatch;
};
/**
* Change working directory
* @param {string} dir
*/
export function cd(dir: string): void;
/**
* This function pauses or "sleeps" code execution for a specified duration.
* @param {string|number} duration - The duration to pause execution for, e.g., '100ms' or '3s'.
*
* @example
*
* await sleep('5s');
*/
export function sleep(duration: string | number): Promise<any>;
/**
* Retries a given asynchronous function a specified number of times with optional delays between attempts.
*
* @param {number} count - The number of retry attempts.
* @param {string|expBackoff|Function} a - Either a delay duration as a string, a delay generator object, or the callback function.
* @param {Function} [b] - The callback function to retry, required if `a` is not a function.
* @returns {Promise<*>} - The result of the callback function if it succeeds within the retry attempts.
* @throws {Error} - The last error encountered if all retry attempts fail.
*
* @example
* // Retry a command 3 times
* const p = await retry(3, () => SH`curl -s https://flipwrsi`);
*
* // Retry a command 3 times with an interval of 1 second between each try
* const p = await retry(3, '1s', () => SH`curl -s https://flipwrsi`);
*
* // Retry a command 3 times with irregular intervals using exponential backoff
* const p = await retry(3, expBackoff(), () => SH`curl -s https://flipwrsi`);
*/
export function retry(count: number, a: string | typeof expBackoff | Function, b?: Function | undefined): Promise<any>;
/**
* This function reads the standard input (stdin) from the current process.
* @example
* const content = await stdin();
*/
export function readIn(): Promise<string>;
/**
* Create a async/sync context in new execution callstack
* @param {function} callback - async/sync function
* @param {ResolveCallback} [resolve] - optional resolve/result callback
* @param {RejectCallback} [reject] - optional reject/error callback
* @example
* const p = within(async () => {
*		const res = await Promise.all([
*			SH`sleep 1; echo 1`.run(),
*			SH`sleep 2; echo 2`.run(),
*			sleep(2),
*			SH`sleep 3; echo 3`.run()
*		]);
*   return 'res';
* });
*/
export function within(callback: Function, resolve?: Function | undefined, reject?: Function | undefined): void;
/**
 * Generates an exponential backoff time with a random jitter.
 *
 * @generator
 * @param {string} [max='60s'] - The maximum backoff time in a human-readable format (e.g., '60s' for 60 seconds).
 * @param {string} [rand='100ms'] - The maximum random jitter time in a human-readable format (e.g., '100ms' for 100 milliseconds).
 * @yields {number} The backoff time in milliseconds.
 */
export function expBackoff(max?: string | undefined, rand?: string | undefined): Generator<number, void, unknown>;
/**
* Parses command-line arguments into an object.
*
* The function recognizes arguments that start with two dashes (`--`) as keys,
* and the subsequent value (if not another key) as the corresponding value.
* If a key does not have a value, it defaults to `true`.
* All unrecognized arguments are collected in an array under the `_` property.
*
* @param {string[]} args - An array of command-line arguments.
* @returns {ArgsObject} An object where:
*   - Each key corresponds to an argument that starts with `--`,
*   - The value is either the next argument or `true` if no value is provided,
*   - The `_` property contains an array of unbound arguments.
*/
export function parseArgs(args: string[]): ArgsObject;
/**
* @typedef {Object.<string, ApiMethod>} ArgsObject
* @property {string} [key: string] - Any string key maps to an object
* @property {string[]} _ - Array of strings, unnamed parameters
* @description Parse parameters result.
*/
/**
* @typedef {Function} RejectCallback
* @param {Error} error - The error object passed to the callback.
*/
/**
* @typedef {Function} ResolveCallback
* @param {any} [param] - Optional callback any value
*/
/**
* Creates a new SHDispatch object that represents a command to be executed.
*
* @typedef {Function} Shell
* @property {function(Array, ...*): ProcessPromise} execute - The function to execute the command.
*
* @param {Array} pieces - An array of string literals from a template literal.
* @param {...*} args - The values to be interpolated into the string literals.
* @returns {SHDispatch} Trigger for the command.
* @throws {Error} Throws an error if any of the string literals in `pieces` is undefined.
*
* @example
* const command = await SH`echo 'Hello, world!'`.run();
*/
/**
* Determine a javascript type
*
* @param {any} fn - Any let type
* @returns {string} The "real" object / typeof name
*/
export function jsType(fn: any): string;
import Test from './Test.js';
import assert from 'node:assert';
import SHDispatch from './SHDispatch.js';
export { Test, assert };
